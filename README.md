# An Ansible playbook for webapp - an example repo

You can run it with:

```shell
python3 -m venv venv
source venv/bin/activate
pip install ansible
ansible-playbook -i inventory devbook.yml
```
